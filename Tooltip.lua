--[[
	Looter v<%version%>
	Tooltip Functions
	
	Revision: $Id: Tooltip.lua 6 2016-10-01 08:44:02 Pacific Daylight Time Kjasi $
]]

local l = _G.Looter
if (not l) then
	print(RED_FONT_COLOR_CODE.."Tooltip is unable to find Looter's Global."..FONT_COLOR_CODE_CLOSE)
	return
end

local L = l.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Tooltip is unable to find Looter's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end

local LibKJ = LibStub("LibKjasi-1.0")

-- Add data to Mob's Tooltip
function Looter_TooltipAddMobData(self)
	if not self then return end
	local _, unit = self:GetUnit()
	if not unit then return end
	local GUID = UnitGUID(unit)
	if not GUID then return end
	local ut, uID, sID = l:GetGUIDInfo(GUID)
	if ut ~= "Creature" and ut ~= "Vehicle" then return end
	local mUID = l:BuildMobID(uID,{GetInstanceInfo()})
	if not l.db.Mobs[mUID] then return end
	local Mob = l:TooltipGetMobData(mUID)
	--LibKJ:printr(Mob)
	local _, class = UnitClass("player")
	local isRogue = ((class == "ROGUE") or (l.opt.AlwaysShowPicks == true))
	if not Mob or ((Mob.Money == 0 and LibKJ:tcount(Mob.Loot) == 0 and LibKJ:tcount(Mob.Skin) == 0) and (isRogue and (Mob.PickPocket.Money == 0 and LibKJ:tcount(Mob.PickPocket.Loot) == 0))) then return end
	if (l.opt.AlwaysShowPicks == false) and (Mob.TimesLooted == 0 and Mob.TimesSkinned == 0 and Mob.TimesPicked > 0) then return end
	
	local MobItemListCurrent = 0
	
	-- Add the data to the tooltip
	if l.opt.AddSpacer then 
		self:AddLine(" ")
	end
	
	-- Title and Total Loots
	local LootString = format(L["ToolTip Loots Made"],Mob.TimesLooted)
	local PickString = format(L["ToolTip Picks Made"],Mob.TimesPicked)
	local SkinString = format(L["ToolTip Skins Made"],Mob.TimesSkinned)
	if Mob.TimesLooted == 1 then LootString = format(L["ToolTip Loots Made Single"],Mob.TimesLooted) end
	if Mob.TimesPicked == 1 then LootString = format(L["ToolTip Picks Made Single"],Mob.TimesPicked) end
	if Mob.TimesSkinned == 1 then SkinString = format(L["ToolTip Skins Made Single"],Mob.TimesSkinned) end
	local FinalLootString = LootString
	local LootCounter = Mob.TimesLooted
	if isRogue then
		if Mob.TimesLooted > 0 and Mob.TimesSkinned > 0 and Mob.TimesPicked > 0 then
			FinalLootString = format(L["Loot Triple Comma"],LootString,PickString,SkinString)
			LootCounter = (Mob.TimesLooted + Mob.TimesSkinned + Mob.TimesPicked)
		elseif Mob.TimesLooted > 0 and Mob.TimesPicked > 0 then
			FinalLootString = format(L["Loot Double Comma"],LootString,PickString)
			LootCounter = (Mob.TimesLooted + Mob.TimesPicked)
		elseif Mob.TimesLooted > 0 and Mob.TimesSkinned > 0 then
			FinalLootString = format(L["Loot Double Comma"],LootString,SkinString)
			LootCounter = (Mob.TimesLooted + Mob.TimesSkinned)
		elseif Mob.TotalSkins > 0 and Mob.TimesPicked > 0 then
			FinalLootString = format(L["Loot Double Comma"],PickString,SkinString)
			LootCounter = Mob.TimesSkinned + Mob.TimesPicked
		elseif Mob.TimesPicked > 0 then
			FinalLootString = PickString
			LootCounter = Mob.TimesPicked
		elseif Mob.TotalSkins > 0 then
			FinalLootString = SkinString
			LootCounter = Mob.TimesSkinned
		end
	else
		if Mob.TimesLooted > 0 and Mob.TimesSkinned > 0 then
			FinalLootString = format(L["Loot Double Comma"],LootString,SkinString)
			LootCounter = (Mob.TimesLooted + Mob.TimesSkinned)
		elseif Mob.TotalSkins > 0 then
			FinalLootString = SkinString
			LootCounter = Mob.TimesSkinned
		end
	end
	self:AddDoubleLine(l:ColorString(l.TitleColor,L["ToolTip Looter Title"]),l:ColorString(HIGHLIGHT_FONT_COLOR_CODE,FinalLootString))
	
	-- Get Item Qualities
	if l.opt.ShowItemQualities then 
		if Mob.TotalLoots > 0 or Mob.TotalSkins > 0 then
			local ItemQualityList = ""
			for Quality,Count in LibKJ:PairsByKeys(Mob.Quality,function(a,b) return a>b end) do
				local _,_,_,HexColor = GetItemQualityColor(Quality)
				local lc = ((LootCounter - Mob.UncountedLoots) - Mob.UncountedSkins) - Mob.UncountedPicks
				local percent = LibKJ:Round((Count/lc)*100,1)
				if tostring(percent) == "1.#INF" then percent = 100 end
				if ItemQualityList == "" then
					ItemQualityList = l:ColorString(HexColor,format(L["Loot Percentage"],tostring(Count),tostring(percent)))
				else
					ItemQualityList = ItemQualityList.." "..l:ColorString(HexColor,format(L["Loot Percentage"],tostring(Count),tostring(percent)))
				end
			end
			self:AddDoubleLine(l:ColorString(l.DataColor,L["ToolTip Quality"]),ItemQualityList)
		end
	end
	
	-- Money
	if isRogue or l.opt.AlwaysShowPicks == true then
		if Mob.Money > 0 and Mob.PickPocket.Money > 0 then
			local m = format(L["Loot Double Comma"],GetCoinTextureString(Mob.Money,14),format(L["ToolTip Gold Picked"],GetCoinText(Mob.PickPocket.Money)))
			self:AddDoubleLine(l:ColorString(l.DataColor,L["Gold"]),l:ColorString("ffffff",m))
		elseif Mob.PickPocket.Money > 0 then
			self:AddDoubleLine(l:ColorString(l.DataColor,L["Gold"]),l:ColorString("ffffff",format(L["ToolTip Gold Picked"],GetCoinText(Mob.PickPocket.Money))))
		elseif Mob.Money > 0 then
			self:AddDoubleLine(l:ColorString(l.DataColor,L["Gold"]),l:ColorString("ffffff",GetCoinText(Mob.Money)))
		end
	else
		--print("Money for "..Mob.Name.." is "..tostring(tonumber(Mob.Money))..", > 0: "..tostring((tonumber(Mob.Money) > 0)))
		if tonumber(Mob.Money) > 0 then
			self:AddDoubleLine(l:ColorString(l.DataColor,L["Gold"]),l:ColorString("ffffff",GetCoinText(Mob.Money)))
		end
	end

	-- Loot
	if Mob.TotalLoots > 0 then
		self:AddLine(l:ColorString(l.DataColor,L["ToolTip LootList"]))
		for _,Data in LibKJ:PairsByKeys(Mob.Loot,function(a,b) return a>b end) do
			if Data.Quality > 0 then
				if MobItemListCurrent >= l.opt.MobItemListLimit then break end
				if not Data.IsQuestItem or (Data.IsQuestItem and l.opt.ShowQuestItems == true) then
					local percent = LibKJ:Round(Data.Percentage*100)
					local value = format(L["Loot Percentage"],Data.Count,percent)
					if Data.Uncounted then
						value = Data.Count
					end
					self:AddDoubleLine(l:ColorString(Data.HexColor,format(L["Loot Indent"],tostring(Data.Name))),l:ColorString(Data.HexColor,value))
					MobItemListCurrent = MobItemListCurrent + 1
				end
			end
		end
		if Mob.LootCutoffCount > 0 then
			--print("MobCOC: "..Mob.LootCutoffCount.." / Total Loots: "..Mob.TotalLoots)
			local itemname = format(L["Loot Indent"],L["ToolTip Trash Items"])
			local percent = LibKJ:Round((Mob.LootCutoffCount/Mob.TimesLooted)*100)
			local value = format(L["Loot Percentage"],Mob.LootCutoffTotal,percent)
			local _,_,_,HexColor = GetItemQualityColor(0)
			self:AddDoubleLine(l:ColorString(HexColor,itemname),l:ColorString(HexColor,value))
		end
	end
	
	-- Picks
	if isRogue or l.opt.AlwaysShowPicks == true then
		MobItemListCurrent = 0
		if Mob.TotalPicks > 0 then
			self:AddLine(l:ColorString(l.DataColor,L["ToolTip PickList"]))
			for _,Data in LibKJ:PairsByKeys(Mob.PickPocket.Loot,function(a,b) return a>b end) do
				if Data.Quality > 0 then
					if MobItemListCurrent >= l.opt.MobItemListLimit then break end
					if not Data.IsQuestItem or (Data.IsQuestItem and l.opt.ShowQuestItems == true) then
						local percent = LibKJ:Round(Data.Percentage*100)
						local value = format(L["Loot Percentage"],Data.Count,percent)
						if Data.Uncounted then
							value = Data.Count
						end
						self:AddDoubleLine(l:ColorString(Data.HexColor,format(L["Loot Indent"],tostring(Data.Name))),l:ColorString(Data.HexColor,value))
						MobItemListCurrent = MobItemListCurrent + 1
					end
				end
			end
			if Mob.PPLootCutoffCount > 0 then
				--print("MobCOC: "..Mob.PPLootCutoffCount.." / Total Loots: "..Mob.TotalLoots)
				local itemname = format(L["Loot Indent"],L["ToolTip Trash Items"])
				local percent = LibKJ:Round((Mob.PPLootCutoffCount/Mob.TimesPicked)*100)
				local value = format(L["Loot Percentage"],Mob.PPLootCutoffTotal,percent)
				local _,_,_,HexColor = GetItemQualityColor(0)
				self:AddDoubleLine(l:ColorString(HexColor,itemname),l:ColorString(HexColor,value))
			end
		end
	end
	
	-- Skins
	MobItemListCurrent = 0
	if Mob.TotalSkins > 0 then
		local Indent = L["Loot Indent"]
		if l.opt.MergeLists then
			Indent = L["Skin Indent"]
		else
			self:AddLine(l:ColorString(l.DataColor,L["ToolTip SkinList"]))
		end
		local cutoffpercent = 0
		for _,Data in LibKJ:PairsByKeys(Mob.Skin,function(a,b) return a>b end) do
			if Data.Quality > 0 then
				if MobItemListCurrent >= l.opt.MobSkinListLimit then break end
				if not Data.IsQuestItem or (Data.IsQuestItem and l.opt.ShowQuestItems == true) then
					local percent = LibKJ:Round(Data.Percentage*100)
					local value = format(L["Loot Percentage"],Data.Count,percent)
					if Data.Uncounted then
						value = Data.Count
					end
					self:AddDoubleLine(l:ColorString(Data.HexColor,format(Indent,tostring(Data.Name))),l:ColorString(Data.HexColor,value))
					MobItemListCurrent = MobItemListCurrent + 1
				end
			else
				cutoffpercent = cutoffpercent + Data.Percentage
			end
		end
		if Mob.SkinCutoffTotal > 0 then
			--print("MobCOC: "..Mob.SkinCutoffTotal.." / Total Loots: "..Mob.TotalLoots)
			local itemname = format(Indent,L["ToolTip Trash Items"])
			local percent = LibKJ:Round(cutoffpercent*100)
			local value = format(L["Loot Percentage"],Mob.SkinCutoffTotal,percent)
			local _,_,_,HexColor = GetItemQualityColor(0)
			self:AddDoubleLine(l:ColorString(HexColor,itemname),l:ColorString(HexColor,value))
		end
	end
end

-- Add data to Item's Tooltip
function Looter_TooltipAddItemData(self)
	if not self then return end
	_, ItemLink = self:GetItem()
	if not ItemLink then return end
	local dbItemID = l:getID(ItemLink)
	if not dbItemID then return end
	local Item = l:TooltipGetItemData(dbItemID)
	if not Item then return end
	
	local ItemMobListCurrent = 0
	local SkinMobListCurrent = 0
	
	-- Add the data to the tooltip
	if l.opt.AddSpacer then 
		self:AddLine(" ")
	end
	
	if Item.LootCount > 0 then
		self:AddLine(L["ToolTip Looted From"])
		for _,Data in LibKJ:PairsByKeys(Item.Loot,function(a,b) return a>b end) do
			if ItemMobListCurrent >= l.opt.ItemMobListLimit then break end
			local value = format(L["Loot Percentage"],Data.Drops,Data.Percent)
			self:AddDoubleLine(l:ColorString(l.DataColor,Data.MobName),l:ColorString(l.DataColor,value))
			ItemMobListCurrent = ItemMobListCurrent + 1
		end
	end	
	if Item.SkinCount > 0 then
		self:AddLine(L["ToolTip Skinned From"])
		for _,Data in LibKJ:PairsByKeys(Item.Skin,function(a,b) return a>b end) do
			if SkinMobListCurrent >= l.opt.SkinMobListLimit then break end
			local value = format(L["Loot Percentage"],Data.Drops,Data.Percent)
			self:AddDoubleLine(l:ColorString(l.DataColor,Data.MobName),l:ColorString(l.DataColor,value))
			SkinMobListCurrent = SkinMobListCurrent + 1
		end
	end
end

-- Get the data for the Mob Tooltip
function l:TooltipGetMobData(UnitID)
	-- Build the data we need for this tooltip
	local m = l.db.Mobs[UnitID]
	local mdb = {}
	local MoneyCount = 0
	local MoneyPickCount = 0
	
	-- Money
	mdb.Money = 0
	mdb.Name = m.Name
	mdb.Loot = {}
	mdb.Skin = {}
	mdb.PickPocket = {}
	mdb.PickPocket.Money = 0
	mdb.PickPocket.Loot = {}
	mdb.Quality = {}
	mdb.TimesLooted = m.LootCounter
	mdb.TimesPicked = m.PPCounter
	mdb.TimesSkinned = m.SkinCounter
	mdb.LootCutoffTotal = 0
	mdb.LootCutoffCount = 0
	mdb.PPLootCutoffTotal = 0
	mdb.PPLootCutoffCount = 0
	mdb.SkinCutoffTotal = 0
	mdb.SkinCutoffCount = 0
	mdb.UncountedLoots = 0
	mdb.UncountedPicks = 0
	mdb.UncountedSkins = 0
	
	if (#m.Money > 0) then
		for _,val in pairs(m.Money) do
			mdb.Money = mdb.Money + val
			MoneyCount = MoneyCount + 1
		end
		local money = mdb.Money/MoneyCount
		mdb.Money = LibKJ.Floor(nil,money,0)
	end
	
	for _,val in pairs(m.PickPockets.Money) do
		mdb.PickPocket.Money = mdb.PickPocket.Money + val
		MoneyPickCount = MoneyPickCount + 1
	end
	local ppmoney = mdb.PickPocket.Money/MoneyPickCount
	mdb.PickPocket.Money = LibKJ.Floor(nil,ppmoney,0)
	
	for Item,LootData in pairs(m.Loots) do
		local ItemData = l.db.Loot[Item]
		local ItemID = LibKJ:getIDNumber(ItemData.Link)
		local ItemName = GetItemInfo(ItemID)
		local LargestValue = 0
		local dbName = ""
		for _,val in pairs(LootData.Data) do
			if val > LargestValue then LargestValue = val end
		end
		local LVLen = strlen(tostring(LargestValue))
		for num,val in pairs(LootData.Data) do
			local pcent = val/m.LootCounter
			dbName = tostring(pcent).."_"..tostring(ItemName).."_"..num
			mdb.Loot[dbName] = {}
			mdb.Loot[dbName].Name = ItemName
			mdb.Loot[dbName].Uncounted = false
			if tonumber(num) > 1 then
				mdb.Loot[dbName].Name = format(L["Item Multipler"],tostring(ItemName),num)
			end
			if ItemData.IsQuestItem then
				mdb.Loot[dbName].Name = format(L["Quest Item"],tostring(mdb.Loot[dbName].Name))
				mdb.UncountedLoots = mdb.UncountedLoots + 1
				mdb.Loot[dbName].Uncounted = true
			elseif ItemData.StartsQuest ~= false then
				mdb.Loot[dbName].Name = format(L["Start Quest Item"],tostring(mdb.Loot[dbName].Name))
				mdb.UncountedLoots = mdb.UncountedLoots + 1
				mdb.Loot[dbName].Uncounted = true
			end
			mdb.Loot[dbName].Quality = ItemData.Quality
			local _,_,_,hc = GetItemQualityColor(ItemData.Quality)
			mdb.Loot[dbName].HexColor = hc
			mdb.Loot[dbName].IsQuestItem = ItemData.IsQuestItem
			mdb.Loot[dbName].StartsQuest = ItemData.StartsQuest
			mdb.Loot[dbName].Count = val
			mdb.Loot[dbName].Total = tonumber(num) * val
			mdb.Loot[dbName].Percentage = pcent
			if not ItemData.IsQuestItem and not ItemData.StartsQuest then
				if not mdb.Quality[ItemData.Quality] then mdb.Quality[ItemData.Quality] = 0 end
				mdb.Quality[ItemData.Quality] = mdb.Quality[ItemData.Quality] + val
				if ItemData.Quality <= l.opt.DisplayCutOffLimit then
					mdb.LootCutoffTotal = mdb.LootCutoffTotal + mdb.Loot[dbName].Total
					mdb.LootCutoffCount = mdb.LootCutoffCount + val
				end
			end
		end
	end
	for CurrID,CurrData in pairs(m.Currency) do
		local ItemData = l.db.Currency[CurrID]
		local dbName = ""
		for num,val in pairs(CurrData.Data) do
			local CurrName = GetCurrencyInfo(CurrID)
			local pcent = val/m.LootCounter
			dbName = tostring(pcent).."_"..tostring(CurrName).."_"..num
			mdb.Loot[dbName] = {}
			mdb.Loot[dbName].Name = CurrName
			if tonumber(num) > 1 then
				mdb.Loot[dbName].Name = format(L["Item Multipler"],tostring(CurrName),num)
			end
			mdb.Loot[dbName].Uncounted = false
			mdb.Loot[dbName].Quality = ItemData.Quality
			local _,_,_,hc = GetItemQualityColor(ItemData.Quality)
			mdb.Loot[dbName].HexColor = hc
			mdb.Loot[dbName].IsQuestItem = false
			mdb.Loot[dbName].StartsQuest = false
			mdb.Loot[dbName].Count = val
			mdb.Loot[dbName].Total = tonumber(num) * val
			mdb.Loot[dbName].Percentage = pcent
			if (not l.opt.IncludeCurrencyInTotals) then
				mdb.UncountedLoots = mdb.UncountedLoots + 1
			end
			if l.opt.IncludeCurrencyInTotals then
				if not mdb.Quality[ItemData.Quality] then mdb.Quality[ItemData.Quality] = 0 end
				mdb.Quality[ItemData.Quality] = mdb.Quality[ItemData.Quality] + val
				if ItemData.Quality <= l.opt.DisplayCutOffLimit then
					mdb.LootCutoffTotal = mdb.LootCutoffTotal + mdb.Loot[dbName].Total
					mdb.LootCutoffCount = mdb.LootCutoffCount + val
				end
			end
		end
	end
	-- Pickpocket Items
	local isRogue = ((class == "ROGUE") or (l.opt.AlwaysShowPicks == true))
	if isRogue then
		for Item,LootData in pairs(m.PickPockets.Loots) do
			local ItemData = l.db.Loot[Item]
			local ItemID = LibKJ:getIDNumber(ItemData.Link)
			local ItemName = GetItemInfo(ItemID)
			local LargestValue = 0
			local dbName = ""
			for _,val in pairs(LootData.Data) do
				if val > LargestValue then LargestValue = val end
			end
			local LVLen = strlen(tostring(LargestValue))
			for num,val in pairs(LootData.Data) do
				local pcent = val/m.PPCounter
				dbName = tostring(pcent).."_"..tostring(ItemName).."_"..num
				mdb.PickPocket.Loot[dbName] = {}
				mdb.PickPocket.Loot[dbName].Name = ItemName
				mdb.PickPocket.Loot[dbName].Uncounted = false
				if tonumber(num) > 1 then
					mdb.PickPocket.Loot[dbName].Name = format(L["Item Multipler"],tostring(ItemName),num)
				end
				if ItemData.IsQuestItem then
					mdb.PickPocket.Loot[dbName].Name = format(L["Quest Item"],tostring(mdb.Loot[dbName].Name))
					mdb.UncountedPicks = mdb.UncountedPicks + 1
					mdb.PickPocket.Loot[dbName].Uncounted = true
				elseif ItemData.StartsQuest ~= false then
					mdb.PickPocket.Loot[dbName].Name = format(L["Start Quest Item"],tostring(mdb.Loot[dbName].Name))
					mdb.UncountedPicks = mdb.UncountedPicks + 1
					mdb.PickPocket.Loot[dbName].Uncounted = true
				end
				mdb.PickPocket.Loot[dbName].Quality = ItemData.Quality
				local _,_,_,hc = GetItemQualityColor(ItemData.Quality)
				mdb.PickPocket.Loot[dbName].HexColor = hc
				mdb.PickPocket.Loot[dbName].IsQuestItem = ItemData.IsQuestItem
				mdb.PickPocket.Loot[dbName].StartsQuest = ItemData.StartsQuest
				mdb.PickPocket.Loot[dbName].Count = val
				mdb.PickPocket.Loot[dbName].Total = tonumber(num) * val
				mdb.PickPocket.Loot[dbName].Percentage = pcent
				if not ItemData.IsQuestItem and not ItemData.StartsQuest then
					if not mdb.Quality[ItemData.Quality] then mdb.Quality[ItemData.Quality] = 0 end
					mdb.Quality[ItemData.Quality] = mdb.Quality[ItemData.Quality] + val
					if ItemData.Quality <= l.opt.DisplayCutOffLimit then
						mdb.PPLootCutoffTotal = mdb.PPLootCutoffTotal + mdb.PickPocket.Loot[dbName].Total
						mdb.PPLootCutoffCount = mdb.PPLootCutoffCount + val
					end
				end
			end
		end
		for CurrID,CurrData in pairs(m.PickPockets.Currency) do
			local ItemData = l.db.Currency[CurrID]
			local dbName = ""
			for num,val in pairs(CurrData.Data) do
				local CurrName = GetCurrencyInfo(CurrID)
				local pcent = val/m.PPCounter
				dbName = tostring(pcent).."_"..tostring(CurrName).."_"..num
				mdb.PickPocket.Loot[dbName] = {}
				mdb.PickPocket.Loot[dbName].Name = CurrName
				if tonumber(num) > 1 then
					mdb.Loot[dbName].Name = format(L["Item Multipler"],tostring(CurrName),num)
				end
				mdb.PickPocket.Loot[dbName].Uncounted = false
				mdb.PickPocket.Loot[dbName].Quality = ItemData.Quality
				local _,_,_,hc = GetItemQualityColor(ItemData.Quality)
				mdb.PickPocket.Loot[dbName].HexColor = hc
				mdb.PickPocket.Loot[dbName].IsQuestItem = false
				mdb.PickPocket.Loot[dbName].StartsQuest = false
				mdb.PickPocket.Loot[dbName].Count = val
				mdb.PickPocket.Loot[dbName].Total = tonumber(num) * val
				mdb.PickPocket.Loot[dbName].Percentage = pcent
				if (not l.opt.IncludeCurrencyInTotals) then
					mdb.UncountedPicks = mdb.UncountedPicks + 1
				end
				if l.opt.IncludeCurrencyInTotals then
					if not mdb.Quality[ItemData.Quality] then mdb.Quality[ItemData.Quality] = 0 end
					mdb.Quality[ItemData.Quality] = mdb.Quality[ItemData.Quality] + val
					if ItemData.Quality <= l.opt.DisplayCutOffLimit then
						mdb.PPLootCutoffTotal = mdb.PPLootCutoffTotal + mdb.PickPocket.Loot[dbName].Total
						mdb.PPLootCutoffCount = mdb.PPLootCutoffCount + val
					end
				end
			end
		end
	end
	-- Skinning Data
	for Item,SkinData in pairs(m.Skins) do
		local ItemData = l.db.Loot[Item]
		local ItemID = LibKJ:getIDNumber(ItemData.Link)
		local ItemName = GetItemInfo(ItemID)
		local LargestValue = 0
		local dbName = ""
		for _,val in pairs(SkinData.Data) do
			if val > LargestValue then LargestValue = val end
		end
		local LVLen = strlen(tostring(LargestValue))
		for num,val in pairs(SkinData.Data) do
			local pcent = val/m.SkinCounter
			dbName = tostring(pcent).."_"..tostring(ItemName).."_"..num
			mdb.Skin[dbName] = {}
			mdb.Skin[dbName].Name = ItemName
			mdb.Skin[dbName].Uncounted = false
			if tonumber(num) > 1 then
				mdb.Skin[dbName].Name = format(L["Item Multipler"],tostring(ItemName),num)
			end
			if ItemData.IsQuestItem then
				mdb.Skin[dbName].Name = format(L["Quest Item"],tostring(mdb.Skin[dbName].Name))
				mdb.UncountedSkins = mdb.UncountedSkins + 1
				mdb.Skin[dbName].Uncounted = true
			elseif ItemData.StartsQuest ~= false then
				mdb.Skin[dbName].Name = format(L["Start Quest Item"],tostring(mdb.Skin[dbName].Name))
				mdb.UncountedSkins = mdb.UncountedSkins + 1
				mdb.Skin[dbName].Uncounted = true
			end
			mdb.Skin[dbName].Quality = ItemData.Quality
			local _,_,_,hc = GetItemQualityColor(ItemData.Quality)
			mdb.Skin[dbName].HexColor = hc
			mdb.Skin[dbName].IsQuestItem = ItemData.IsQuestItem
			mdb.Skin[dbName].StartsQuest = ItemData.StartsQuest
			mdb.Skin[dbName].Count = val
			mdb.Skin[dbName].Total = tonumber(num) * val
			mdb.Skin[dbName].Percentage = pcent
			if ((not IncludeSkinsInTotals) and (not mdb.Skin[dbName].Uncounted)) then
				mdb.UncountedSkins = mdb.UncountedSkins + 1
			end
			if not ItemData.IsQuestItem and not ItemData.StartsQuest then
				if l.opt.IncludeSkinsInTotals then
					if not mdb.Quality[ItemData.Quality] then mdb.Quality[ItemData.Quality] = 0 end
					mdb.Quality[ItemData.Quality] = mdb.Quality[ItemData.Quality] + val
					if ItemData.Quality <= l.opt.DisplayCutOffLimit then
						mdb.SkinCutoffTotal = mdb.SkinCutoffTotal + mdb.Skin[dbName].Total
						mdb.SkinCutoffCount = mdb.SkinCutoffCount + val
					end
				end
			end
		end
	end
	
	mdb.TotalLoots = LibKJ:tcount(mdb.Loot)
	mdb.TotalPicks = LibKJ:tcount(mdb.PickPocket.Loot)
	mdb.TotalSkins = LibKJ:tcount(mdb.Skin)
	
	return mdb
end

function l:TooltipGetItemData(DBItemID)
	local i = l.db.Loot[DBItemID]
	if not i then return end
	local idb = {}
	
	idb.Loot = {}
	idb.Skin = {}
	
	for UnitID,MobName in pairs(i.MobList) do
		local Mob = l.db.Mobs[UnitID]
		local LootData = Mob.Loots[DBItemID]
		local SkinData = Mob.Skins[DBItemID]
		
		if LootData then
			local ItemDrops = 0
			for Item, Data in pairs(Mob.Loots) do
				for n,v in pairs(Data.Data) do
					if Item == DBItemID then
						ItemDrops = ItemDrops + (n*v)
					end
				end
			end
			if not l.opt.SeparateSkinData then
				for Item, Data in pairs(Mob.Skins) do
					for n,v in pairs(Data.Data) do
						if Item == DBItemID then
							ItemDrops = ItemDrops + (n*v)
						end
					end
				end
			end
			local IDLen = strlen(tostring(ItemDrops))
			local pcent = ItemDrops/Mob.LootCounter
			local prefix = "1"
			if Mob.LootCounter < l.opt.PercentPriorityLimitLoot then
				prefix = "0"
			end
			local dbName = prefix.."_"..tostring(pcent).."_"..tostring(MobName)..UnitID
			idb.Loot[dbName] = {}
			idb.Loot[dbName].MobName = format(L["Loot Indent"],tostring(MobName))
			idb.Loot[dbName].Drops = ItemDrops
			idb.Loot[dbName].Percent = LibKJ:Round((ItemDrops/Mob.LootCounter)*100)
			if not l.opt.SeparateSkinData then
				idb.Loot[dbName].Percent = LibKJ:Round((ItemDrops/(Mob.LootCounter+Mob.SkinCounter))*100)
			end
		end
		if SkinData and l.opt.SeparateSkinData then
			local ItemDrops = 0
			for Item, Data in pairs(Mob.Skins) do
				for n,v in pairs(Data.Data) do
					if Item == DBItemID then
						ItemDrops = ItemDrops + (n*v)
					end
				end
			end
			local IDLen = strlen(tostring(ItemDrops))
			local pcent = ItemDrops/Mob.SkinCounter
			local prefix = "1"
			if Mob.SkinCounter < l.opt.PercentPriorityLimitSkin then
				prefix = "0"
			end
			local dbName = prefix.."_"..tostring(pcent).."_"..tostring(MobName)..UnitID
			idb.Skin[dbName] = {}
			idb.Skin[dbName].MobName = format(L["Loot Indent"],tostring(MobName))
			idb.Skin[dbName].Drops = ItemDrops
			idb.Skin[dbName].Percent = LibKJ:Round(pcent*100)
		end
	end
	
	idb.LootCount = LibKJ:tcount(idb.Loot)
	idb.SkinCount = LibKJ:tcount(idb.Skin)
	
	return idb
end