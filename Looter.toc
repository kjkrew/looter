## Interface: 70100
## Title: Looter
## Author: Kjasi
## Version: <%version%>
## Notes: Tracks and reports loot. Good for farming!
## URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Website: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Feedback: https://bitbucket.org/Kjasi/kjasis-wow-addons
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: LooterDatabase, LooterOptions
Load.xml