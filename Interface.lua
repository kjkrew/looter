--[[
	Looter v<%version%>
	Interface Functions
	
	Revision: $Id: Interface.lua 1 2015-06-07 18:35:16 Pacific Daylight Time Kjasi $
]]

local l = _G.Looter
if (not l) then
	print(RED_FONT_COLOR_CODE.."Interface is unable to find Looter Global."..FONT_COLOR_CODE_CLOSE)
	return
end
local L = l.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Interface is unable to find Looter's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end
l.Frames = CreateFrame("Frame", "LooterFrames", UIParent)
l.InterfaceOptions = {}
local I = l.InterfaceOptions

function I:onLoad(self)
	-- Register the Interface Options page
	self.name = L["Title"]
	InterfaceOptions_AddCategory(Looter.Frames.InterfaceOptions)

	self.Title:SetText(L["Title"])
	self.Version:SetText(format(L["Version"], l.Version))
end

-- Add Tooltip Data for our Options
function I:SetTooltip(frame, title, text)
	frame.tooltipText = title
	frame.tooltipRequirement = text
end

function I:SetSliderText(self)
	if (not self) then return end
	
	local frameName = "LooterFrames_"..self.option
	
	if (self.option == "DisplayCutOffLimit") then
		local hexValue = l.DataColor
		local strValue = L["Show All"]
		if (self.value > -1) then
			strValue = _G["ITEM_QUALITY"..self.value.."_DESC"]
			_,_,_,hexValue = GetItemQualityColor(self.value)
		end
		local _,_,_,hexHigh = GetItemQualityColor(5)
		_G[frameName.."Text"]:SetText(format(L["Loot Double Colon"],L[self.locTitle],l:ColorString(hexValue,strValue)..FONT_COLOR_CODE_CLOSE))
		_G[frameName.."Low"]:SetText(l:ColorString(l.DataColor,L["Show All"])..FONT_COLOR_CODE_CLOSE)
		_G[frameName.."High"]:SetText(l:ColorString(hexHigh,ITEM_QUALITY5_DESC)..FONT_COLOR_CODE_CLOSE)
	else
		local strLow = tostring(L[self.locLow])
		local strHigh = tostring(L[self.locHigh])
		if (self.numLow) then strLow = tostring(self.numLow) end
		if (self.numHigh) then strHigh = tostring(self.numHigh) end
		_G[frameName.."Text"]:SetText(format(L["Loot Double Colon"], L[self.locTitle], self.value))
		_G[frameName.."Low"]:SetText(strLow)
		_G[frameName.."High"]:SetText(strHigh)
	end
end

-- Add option Subcategories
function I:AddCategory(frame, name, parent)
	frame.name = name
	frame.parent = parent
	InterfaceOptions_AddCategory(frame)
end

function I:onShow(self)
	local f = Looter.Frames.InterfaceOptions
	
	-- Set values from Options
	-- Checkboxes
	f.AddSpacer:SetChecked(l.opt.AddSpacer)
	f.MergeLists:SetChecked(l.opt.MergeLists)
	f.ShowQuestItems:SetChecked(l.opt.ShowQuestItems)
	f.AlwaysShowPicks:SetChecked(l.opt.AlwaysShowPicks)
	f.ShowItemQualities:SetChecked(l.opt.ShowItemQualities)
	f.IncludeCurrencyInTotals:SetChecked(l.opt.IncludeCurrencyInTotals)
	f.IncludeSkinsInTotals:SetChecked(l.opt.IncludeSkinsInTotals)
	f.SeparateSkinData:SetChecked(l.opt.SeparateSkinData)
	
	-- Slider Values
	LooterFrames_MobItemListLimit:SetValue(l.opt.MobItemListLimit)
	LooterFrames_MobSkinListLimit:SetValue(l.opt.MobSkinListLimit)
	
	--[[
	LooterFrames_ItemMobListLimit:SetValue(l.opt.ItemMobListLimit)
	LooterFrames_SkinMobListLimit:SetValue(l.opt.SkinMobListLimit)
	LooterFrames_DisplayCutOffLimit:SetValue(l.opt.DisplayCutOffLimit)
	LooterFrames_PercentPriorityLimitLoot:SetValue(l.opt.PercentPriorityLimitLoot)
	LooterFrames_PercentPriorityLimitSkin:SetValue(l.opt.PercentPriorityLimitSkin)
	]]
	
end

-- Checkbox Values
function I:CheckBox(self)
	if (not self) then return end

	local Name = self.option
	local isChecked = self:GetChecked()
	if (isChecked == nil) then
		isChecked = false
	end
	--print("Name: "..tostring(Name))
	--print("isChecked: "..tostring(isChecked))
	if (Name == "AddSpacer") then
		l.opt.AddSpacer = isChecked
	end
	if (Name == "MergeLists") then
		l.opt.MergeLists = isChecked
	end
	if (Name == "ShowQuestItems") then
		l.opt.ShowQuestItems = isChecked
	end
	if (Name == "AlwaysShowPicks") then
		l.opt.AlwaysShowPicks = isChecked
	end
	if (Name == "ShowItemQualities") then
		l.opt.ShowItemQualities = isChecked
	end
	if (Name == "IncludeCurrencyInTotals") then
		l.opt.IncludeCurrencyInTotals = isChecked
	end
	if (Name == "IncludeSkinsInTotals") then
		l.opt.IncludeSkinsInTotals = isChecked
	end
	if (Name == "SeparateSkinData") then
		l.opt.SeparateSkinData = isChecked
	end
end

-- Slider Values
function I:SetSlider(self, value)
	if (not self) then return end
	if (not l.opt) then return end
	if (not value) then print("No value for "..self.option..", using Defaults.")value = l.Defaults[self.option] end

	local Name = self.option
	local fname = self.name;
	--print("Frame: "..tostring(fname))
	value = math.floor(value * 100 + 0.5) / 100
	
	if Name == "MobItemListLimit" then
		l.opt.MobItemListLimit = value
	end
	if Name == "MobSkinListLimit" then
		l.opt.MobSkinListLimit = value
	end
	if Name == "ItemMobListLimit" then
		l.opt.ItemMobListLimit = value
	end
	if Name == "SkinMobListLimit" then
		l.opt.SkinMobListLimit = value
	end
	if Name == "DisplayCutOffLimit" then
		l.opt.DisplayCutOffLimit = value
	end
	if Name == "PercentPriorityLimitLoot" then
		l.opt.PercentPriorityLimitLoot = value
	end
	if Name == "PercentPriorityLimitSkin" then
		l.opt.PercentPriorityLimitSkin = value
	end
	
	I:SetSliderText(self)
end

-- Create Tooltip
function l:CreateTooltip(frame, title, text, anchor)
	if anchor == nil then
		anchor = "ANCHOR_CURSOR"
	end
	if title == nil then
		title = "nil"
	end
	if text == nil then
		text = "nil"
	end
	
	LooterTooltip:SetOwner(frame, anchor)
	LooterTooltip:SetText(title)
	LooterTooltip:AddLine(text,1,1,1,true)
	LooterTooltip:Show()
end