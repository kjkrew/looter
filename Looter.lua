--[[
	Looter
	Version <%version%>

	Revision: $Id: Looter.lua 6 2016-10-01 08:43:36 Pacific Daylight Time Kjasi $
]]

local Looter = _G.Looter
if (not Looter) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find Looter's Global."..FONT_COLOR_CODE_CLOSE)
	return
end

local L = Looter.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find Looter's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end

Looter.Debug = 1

-- Libraries
local LibKJ = LibStub("LibKjasi-1.0")
-- Setup our addon with KjasiLib
LibKJ:setupAddon({
	["Name"] = "Looter",
	["Version"] = Looter.Version,
	["MsgStrings"] = {
		["Default"] = Looter.TitleColor..L["Message Normal"],
		["error"] = RED_FONT_COLOR_CODE..L["Message Error"],
		["debug"] = LIGHTYELLOW_FONT_COLOR_CODE..L["Message Debug"],
	},
	["ChannelLevel"] = Looter.Debug,
})

function Looter_Load(self)
	self:RegisterEvent("VARIABLES_LOADED")
	self:RegisterEvent("LOOT_OPENED")
	self:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")

	SlashCmdList["Looter"] = Looter_CommandLine
	SLASH_Looter1 = "/looter"
end

function Looter_Events(self, event, ...)
	if (event =="VARIABLES_LOADED") then
		Looter:Loaded()
		self:UnregisterEvent("VARIABLES_LOADED")
	elseif (event == "LOOT_OPENED") then
		Looter:RecordLoot()
	elseif (event == "UNIT_SPELLCAST_SUCCEEDED") then
		if select(1,...) ~= "player" then return end
		Looter:CheckSpellCast(select(5,...))
	end
end

function Looter_CommandLine(cmd)
	if cmd == "test" then
		local ne,nq = GetNumQuestLogEntries()
		for i=1,ne do
			SelectQuestLogEntry(i)
			local qid = GetQuestID()
			local qIndex = i
			local qLink = GetQuestLink(qIndex)
			local qText = GetQuestText()
			print(format("i: %s, qid: %s, qIndex: %s, QLink: %s, QItemNum: %s", tostring(i), tostring(qid), tostring(qIndex), tostring(qLink), tostring(GetNumQuestLogRewards())))
			--print(format("text: %s", tostring(qText)))
			

			for j=1,GetNumQuestLogRewards() do
				local name, texture, count, quality, isUsable = GetQuestLogRewardInfo(j)
				print("Required Item for Quest "..tostring(qLink).." #"..tostring(j)..": "..tostring(name))
			end

		end
	else
		InterfaceOptionsFrame_OpenToCategory(Looter.Frames.InterfaceOptions)
	end
end

function Looter:Loaded()
	-- Dev Tool, Wipe out the database
	--LooterDatabase = nil
	
	Looter:BuildDatabases()
	
	Looter.db = LooterDatabase
	Looter.opt = LooterOptions
	GameTooltip:HookScript("OnTooltipSetUnit", Looter_TooltipAddMobData)
	GameTooltip:HookScript("OnTooltipSetItem", Looter_TooltipAddItemData)
	Looter:Msg("Loaded!")
end

-- Main Functionality
function Looter:RecordLoot()
	-- Ignore fishing Loot
	if IsFishingLoot() then return end
	
	local loot = {}
	local rloot = {}
	loot.NumSlots = GetNumLootItems()
	loot.InstanceInfo = {GetInstanceInfo()}
	
	-- grab all the data we need
	for slot=1, GetNumLootItems() do
		local link = GetLootSlotLink(slot)
		if link and not rloot[link] then
			loot[slot] = {}
			loot[slot].Type = GetLootSlotType(slot)
			loot[slot].Info = slotInfo
			loot[slot].SourceInfo = srcInfo
			loot[slot].Link = link
			rloot[link] = true
		end
	end

	-- process the data, now that the time-sensitive part is over
	Looter:ProcessList(loot)
end

-- Proccess our loot table!
function Looter:ProcessList(loot)
	local Mobs = Looter.db.Mobs
	local mldb = Looter.db.MobLootList
	local Date = time()
	local FirstLoot = false
	local LastLooter = Looter:BuildLooterInfo(Date)

	--LibKJ:printr(loot)
	
	-- Build a proper loot List
	local MobLoot = {}
	for slot=1,loot.NumSlots do
		if loot[slot].SourceInfo then
			for GUID,Count in pairs(loot[slot].SourceInfo) do
				--print(loot[slot].Link)
				--LibKJ:printr(loot[slot].SourceInfo)
				local UnitType, UnitID, sUID = Looter:GetGUIDInfo(GUID)
				--LibKJ:printr({Looter:GetGUIDInfo(GUID)})
				if UnitType == "Creature" or UnitType == "Vehicle" then	-- No idea why some Mobs show up as Vehicles, but they do...
					if not MobLoot[GUID] then
						MobLoot[GUID] = {}
						MobLoot[GUID].UnitID = UnitID
						MobLoot[GUID].SpawnID = sUID
						MobLoot[GUID].InstanceType = loot.InstanceInfo[2]
						MobLoot[GUID].InstanceDiff = loot.InstanceInfo[3]
						MobLoot[GUID].Loot = {}
					end
					MobLoot[GUID].Loot[slot] = {}
					MobLoot[GUID].Loot[slot].Info = loot[slot].Info
					MobLoot[GUID].Loot[slot].Link = loot[slot].Link
					MobLoot[GUID].Loot[slot].Type = loot[slot].Type
					MobLoot[GUID].Loot[slot].Value = Count
				end
			end
		end
	end
	
	-- Process List
	for GUID,m in pairs(MobLoot) do
		local NPCName = Looter:GetNPCInfo(GUID)
		-- Setup Mob Data
		local mUID = Looter:BuildMobID(m.UnitID,loot.InstanceInfo)
		if not Mobs[mUID] then
			Mobs[mUID] = {}
			Mobs[mUID].ZoneID = {}
			Mobs[mUID].UnitID = m.UnitID
			Mobs[mUID].Name = NPCName
			Mobs[mUID].Money = {}
			Mobs[mUID].Loots = {}
			Mobs[mUID].PickPockets = {}
			Mobs[mUID].PickPockets.Money = {}
			Mobs[mUID].PickPockets.Loots = {}
			Mobs[mUID].PickPockets.Currency = {}
			Mobs[mUID].Currency = {}
			Mobs[mUID].Skins = {}
			Mobs[mUID].LootCounter = 0
			Mobs[mUID].SkinCounter = 0
			Mobs[mUID].PPCounter = 0
			Mobs[mUID].LastLoot = 0
			Mobs[mUID].LastLootedBy = LastLooter
		end
		if not Mobs[mUID].Name or Mobs[mUID].Name == "nil" then Mobs[mUID].Name = NPCName end
		
		if not mldb[GUID] then
			mldb[GUID] = {}
			mldb[GUID].Time = Date
			FirstLoot = true
			
			-- Iterate our zone loot counter
			local mapID = GetCurrentMapAreaID()
			if not Mobs[mUID].ZoneID[mapID] then Mobs[mUID].ZoneID[mapID] = 0 end
			Mobs[mUID].ZoneID[mapID] = Mobs[mUID].ZoneID[mapID] + 1
		end
		
		if FirstLoot == true or Looter.PickPocketSpellUsed or Looter.SkinningSpellUsed then
			Mobs[mUID].LastLoot = Date
			Mobs[mUID].LastLootedBy = LastLooter
			if Looter.SkinningSpellUsed then
				Mobs[mUID].SkinCounter = Mobs[mUID].SkinCounter + 1
			elseif Looter.PickPocketSpellUsed then
				Mobs[mUID].PPCounter = Mobs[mUID].PPCounter + 1
			else
				Mobs[mUID].LootCounter = Mobs[mUID].LootCounter + 1
			end
			-- Process this mob's Loot
			local lootdb = Looter.db.Loot
			for slot,data in pairs(m.Loot) do
				if data.Type == LOOT_SLOT_ITEM then
					-- Items
					-- Item Data
					local ItemID = Looter:getID(data.Link)
					if not lootdb[ItemID] then
						lootdb[ItemID] = {}
						lootdb[ItemID].Name = data.Info[2]
						lootdb[ItemID].Link = data.Link
						lootdb[ItemID].Quality = data.Info[4]
						lootdb[ItemID].IsQuestItem = data.Info[6] or false
						lootdb[ItemID].StartsQuest = data.Info[7] or false
						lootdb[ItemID].LastLootedBy = LastLooter
						lootdb[ItemID].MobList = {}
					end
					-- Only need to find out which mobs have this item for reverse lookup
					if not lootdb[ItemID].MobList[mUID] then lootdb[ItemID].MobList[mUID] = Mobs[mUID].Name end
					lootdb[ItemID].LastLootedBy = LastLooter
					
					-- Mob Data, were the actual looting data is
					local MobLoots = Mobs[mUID].Loots
					
					if Looter.SkinningSpellUsed then
						MobLoots = Mobs[mUID].Skins
					elseif Looter.PickPocketSpellUsed then
						MobLoots = Mobs[mUID].PickPockets.Loots
					end
					local Value = tostring(data.Value)
					
					if not MobLoots[ItemID] then MobLoots[ItemID] = {} MobLoots[ItemID].Total = 0 MobLoots[ItemID].Count = 0 MobLoots[ItemID].Data = {} end
					if not MobLoots[ItemID].Data[Value] then MobLoots[ItemID].Data[Value] = 0 end
					MobLoots[ItemID].Total = MobLoots[ItemID].Total + data.Value
					MobLoots[ItemID].Count = MobLoots[ItemID].Count + 1
					MobLoots[ItemID].Data[Value] = MobLoots[ItemID].Data[Value] + 1
				elseif data.Type == LOOT_SLOT_MONEY then
					-- Money
					local MobMoney = Mobs[mUID].Money
					if Looter.PickPocketSpellUsed then
						MobMoney = Mobs[mUID].PickPockets.Money
					end
					tinsert(MobMoney, data.Value)
				elseif data.Type == LOOT_SLOT_CURRENCY then
					-- Currency
					local MobCurr = Mobs[mUID].Currency

					if Looter.PickPocketSpellUsed then
						MobCurr = Mobs[mUID].PickPockets.Currency
					end
					local currdb = Looter.db.Currency
					local cID, ctype = Looter:getID(data.Link)
					local Value = data.Info[3]
					local ValString = tostring(Value)
					if ctype == "currency" then
						if not currdb[cID] then
							currdb[cID] = {}
							currdb[cID].Name = data.Info[2]
							currdb[cID].Link = data.Link
							currdb[cID].Quality = data.Info[4]
							currdb[cID].MobList = {}
						end
						-- Only need to find out which mobs have this item for reverse lookup
						if not currdb[cID].MobList[mUID] then currdb[cID].MobList[mUID] = Mobs[mUID].Name end
						if not MobCurr[cID] then MobCurr[cID] = {} MobCurr[cID].Total = 0 MobCurr[cID].Count = 0 MobCurr[cID].Data = {} end
						if not MobCurr[cID].Data[ValString] then MobCurr[cID].Data[ValString] = 0 end
						MobCurr[cID].Total = MobCurr[cID].Total + Value
						MobCurr[cID].Count = MobCurr[cID].Count + 1
						MobCurr[cID].Data[ValString] = MobCurr[cID].Data[ValString] + 1
					end
				end
			end
		end
	end

	if Looter.SkinningSpellUsed then
		Looter.SkinningSpellUsed = nil
	end
	if Looter.PickPocketSpellUsed then
		Looter.PickPocketSpellUsed = nil
	end
end

--== Helper Functions ==--
function Looter:Msg(msg,channel)
	if (msg == nil) then return end
	LibKJ:Msg(L["Title"], msg, channel)
end


function Looter:GetGUIDInfo(guid)
	if not guid then return end
	local g = {strsplit("-", guid)}
	local ut = g[1]
	local uID = g[6]
	local sID = g[7]
	
	return ut, uID, sID
end

function Looter:BuildDatabases()
	if not LooterOptions then LooterOptions = Looter.Defaults end
	if not LooterOptions.Version then LooterOptions.Version = Looter.Version end
	if LooterOptions.Version ~= Looter.Version then
		LibKJ:UpdateDatabase(LooterOptions,Looter.Defaults)
		LooterOptions.Version = Looter.Version
	end
	
	if not LooterDatabase then LooterDatabase = {} end
	if not LooterDatabase["Mobs"] then LooterDatabase["Mobs"] = {} end
	if not LooterDatabase["Loot"] then LooterDatabase["Loot"] = {} end
	if not LooterDatabase["Currency"] then LooterDatabase["Currency"] = {} end
	if not LooterDatabase["MobLootList"] then LooterDatabase["MobLootList"] = {} end
	Looter:CleanMobLootList()
end

-- Clean Database of old Mobs (over 1 hour old)
function Looter:CleanMobLootList()
	local OldTime = 3600	-- 1 hour in seconds
	local cTime = time() 	-- Current Time

	-- Clean Database of old Mobs (over 1 hour old)
	local CleanMLL = {}
	for GUID,mlldata in pairs(LooterDatabase["MobLootList"]) do
		local RecTime = mlldata.Time
		if RecTime > (cTime-OldTime) then
			CleanMLL[GUID] = mlldata
		end
	end
	LooterDatabase["MobLootList"] = CleanMLL
end

-- Will take an array of data, and return an array of [First] = Second
function Looter:DataAsPairs(InputData)
	if not InputData or type(InputData) ~= "table" then return end
	local count = #InputData
	local OutputData = {}
	
	for i=1,count,2 do
		OutputData[InputData[i]] = InputData[i+1]
	end
	
	return OutputData
end

-- returns ID, type
function Looter:getID(link)
	if not link then return end
	
	local raw = string.gsub(link,".-\124H([^\124]*)\124h.-\124h\124r", "%1")
	local v = {strsplit(":",raw)}
	local id = nil
	if v[1] == "currency" then
		id = v[2]
	else
		id = strjoin(":",v[2],v[8],v[12])
	end
	
	return id, v[1]
end

function Looter:CheckSpellCast(SpellID)
	if not SpellID then Looter.SkinningSpellUsed = nil Looter.PickPocketSpellUsed = nil return end
	--print("Checking Spell with ID: "..tostring(SpellID))
	if Looter.SkinningSpellList[SpellID] then
		--Looter:Msg(format("Skinning Skill used with an ID of %s!",tostring(SpellID)),"debug",1)
		Looter.SkinningSpellUsed = SpellID
	end
	if Looter.PickPocketSkillID == SpellID then
		Looter.PickPocketSpellUsed = SpellID
	end
end

function Looter:GetNPCInfo(GUID)
	local f = CreateFrame('GameTooltip', 'LooterScanningTooltip', UIParent, 'GameTooltipTemplate')
	f:SetOwner(UIParent, "ANCHOR_NONE")
	
	local unitstring = format("unit:%s", GUID)
	f:SetHyperlink(unitstring)
	
	local name = f:GetUnit()
	f:Hide()
	return tostring(name)
end

function Looter:ColorString(HexColor, String)
	if not String then return end
	if not HexColor then return String end
	local hex = strmatch(HexColor,"c([0-9a-fA-F]{6,8})")
	if not hex then hex = HexColor end
	if strlen(hex) == 6 then
		hex = "ff"..hex
	end
	if strlen(hex) == 8 then
		hex = "|c"..hex
	end
	return tostring(format("%s%s|r",hex,String))
end

function Looter:BuildLooterInfo(LootTime)
	if not LootTime then LootTime = time() end
	local form = "%s_%s_%s_%s_%s_%s"
	local _,class = UnitClass("player")
	local Looter = format(form,UnitFactionGroup("player"),GetRealmName("plyaer"),UnitName("player"),class,tostring(UnitLevel("player")),tostring(LootTime))
	return Looter
end

function Looter:GetLooterInfo(LootPlayerString)
	local a = strsplit("_",LootPlayerString)
	return a[1], a[2], a[3], a[4], a[5], a[6]
end

function Looter:BuildMobID(UnitID,InstanceInfo)
	if not InstanceInfo then InstanceInfo = {GetInstanceInfo()} end
	-- Loots made in garrisons should not be separate.
	if InstanceInfo[8] == 976 or InstanceInfo[8] == 971 then
		InstanceInfo[2] = "none"
		InstanceInfo[3] = 0
	end
	return UnitID..InstanceInfo[2]..InstanceInfo[3]
end