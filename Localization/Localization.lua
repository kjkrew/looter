﻿--[[
	Looter v<%version%>
	Primary Localization
	
	This file contains localiation data that is critical to Looter's functions. Decorative localizations that do not impact function, can be found in the Localization directory.
	
	Revision: $Id: Localiation.lua 4 2015-06-08 17:05:17 Pacific Daylight Time Kjasi $
]]

local lt = _G.Looter
if (not lt) then
	print(RED_FONT_COLOR_CODE.."Unable to find Looter Global."..FONT_COLOR_CODE_CLOSE)
	return
end

lt.Localization = {}
local L = lt.Localization
local Locale = GetLocale()

-- Looter Names & Titles
L["Title"] = "Looter"

-- [[ enUS & enGB ]] --
L["Message Normal"] = "%s:"..FONT_COLOR_CODE_CLOSE.." %s"		-- Addon title, then Message = "Looter: Message"
L["Message Error"] = "%s Error:"..FONT_COLOR_CODE_CLOSE.." %s"	-- Addon title, then Message = "Looter Error: Error Message"
L["Message Debug"] = "%s Debug:"..FONT_COLOR_CODE_CLOSE.." %s"	-- Addon title, then Message = "Looter Debug Message: Debug Message"

L["Loot Percentage"] = "%s (%s%%)"		-- Variables: Number of times looted, Percentage of loots.
L["Loot Double Colon"] = "%s: %s"		-- Variables: Two strings, separated by a colon.
L["Loot Double Comma"] = "%s, %s"		-- Variables: Two strings, separated by a comma.
L["Loot Triple Comma"] = "%s, %s, %s"	-- Variables: Three strings, separated by a comma.
L["Loot Indent"] = "   %s"				-- Item Name
L["Skin Indent"] = "   *%s"				-- Item Name, used only when Merging Skinning and Looting lists
L["Item Multipler"] = "%s (x%d)"		-- Variables: Item Name, Number that dropped at once. Use for when an item drops 2 or more at once. Example: Wool Cloth (x3)
L["Quest Item"] = "%s (Q)"				-- Indicates that the item is part of a quest
L["Start Quest Item"] = "%s (SQ)"		-- Indicates that the item starts a quest