--[[
	Looter v<%version%>
	American English (enUS) Localization
	
	Revision: $Id: enUS.lua 3 2015-06-08 19:35:51 Pacific Daylight Time Kjasi $
]]

local l = _G.Looter
if (not l) then
	print(RED_FONT_COLOR_CODE.."enUS Localization is unable to find Looter Global."..FONT_COLOR_CODE_CLOSE)
	return
end
local L = l.Localization
if (not L) then
	print(RED_FONT_COLOR_CODE.."enUS Localization is unable to find Looter's Localization data."..FONT_COLOR_CODE_CLOSE)
	return
end

-- Message Templates
L["Msg Default"] = "|CFF57A5BB%s:"..FONT_COLOR_CODE_CLOSE.." %s"
L["Msg Error"] = "|CFFFF2020%s Error:"..FONT_COLOR_CODE_CLOSE.." %s"

-- Common Items
L["Gold"] = "Average Gold"
L["Show All"] = "Show All"

-- Tooltip related
L["ToolTip Looter Title"] = "Looter Data:"
L["ToolTip Loots Made"] = "%d Loots"			-- Variable: Number of items looted
L["ToolTip Loots Made Single"] = "%d Loot"		-- Variable: A Single loot was recorded
L["ToolTip Picks Made"] = "%d Picks"			-- Variable: Number of items pick pocketed
L["ToolTip Picks Made Single"] = "%d Pick"		-- Variable: A Single pick pocket was recorded
L["ToolTip Skins Made"] = "%d Skins"			-- Variable: Number of items looted from skinning
L["ToolTip Skins Made Single"] = "%d Skin"		-- Variable: A Single Skin was recorded
L["ToolTip Quality"] = "Qualities"
L["ToolTip LootList"] = "Loot List:"
L["ToolTip PickList"] = "Pick Pocketed List:"
L["ToolTip SkinList"] = "Skinning List:"
L["ToolTip Trash Items"] = "Trash Items"		-- Used to list all the items that are below the display cutoff
L["ToolTip Looted From"] = "Looted From:"
L["ToolTip Skinned From"] = "Skinned From:"
L["ToolTip Gold Picked"] = "%s Picked"

-- Interface Options
L["Version"] = "Version %s"
L["Options Add Spacer"] = "Add Spacer"
L["Options Add Spacer Tooltip"] = "Add a blank line to the tooltip before adding Looter data."
L["Options Merge Lists"] = "Merge Displayed Lists"
L["Options Merge Lists Tooltip"] = "Consolidates Loot and Skin data into a single list when looking at a mob.\n\nWhen active, displayed skinning data will respect the Mob Item List Limit option."
L["Options Show Quest Items"] = "Show Quest Items"
L["Options Show Quest Items Tooltip"] = "Displays items that are parts of a questline in the mob's loot data. Also shows items that start quests."
L["Options Show Picks"] = "Always Show Pick Pocket data"
L["Options Show Picks Tooltip"] = "When enabled, all character classes can see pick pocket data, not just rogues."
L["Options Show Qualities"] = "Show Item Qualities"
L["Options Show Qualities Tooltip"] = "Adds Item Quality information to the Mob's Tooltip."
L["Options Include Currency"] = "Include Currencies in Totals"
L["Options Include Currency Tooltip"] = "Includes data from currencies in item totals."
L["Options Include Skins"] = "Include Skins in Totals"
L["Options Include Skins Tooltip"] = "Includes Skinning data in item totals."
L["Options Item Separate Skins"] = "Separate Skinning Data"
L["Options Item Separate Skins Tooltip"] = "Separates Skin data from other loot data when looking at an item."
L["Option Mob Item List Limit"] = "Mob Item List Limit"
L["Option Mob Item List Limit Tooltip"] = "The maximum number of items to list on a mob's tooltip."
L["Option Mob Skin List Limit"] = "Mob Skin List Limit"
L["Option Mob Skin List Limit Tooltip"] = "The maximum number of skins to list on a mob's tooltip."
L["Option Item Mob List Limit"] = "Item Mob List Limit"
L["Option Item Mob List Limit Tooltip"] = "The maximum number of mobs to list on an item's tooltip."
L["Option Skin Mob List Limit"] = "Skin Mob List Limit"
L["Option Skin Mob List Limit Tooltip"] = "The maximum number of mobs to list on a skinning item's tooltip."
L["Option Display Cutoff Limit"] = "Quality Cutoff Limit"
L["Option Display Cutoff Limit Tooltip"] = "Items of this quality or below will be removed from a mob's tooltip. Removed items will be reported as trash.\n\nThis only affects the display of data on the tooltip! Looter will continue to record data for these items."