--[[
	Looter v<%version%>
	Constants
	
	Revision: $Id: Constants.lua 4 2016-10-01 08:40:46 Pacific Daylight Time Kjasi $
]]

Looter = {}
local l = Looter

l.Version = "<%version%>"
l.db = {}
l.SkinningSpellUsed = nil

l.TitleColor = ORANGE_FONT_COLOR_CODE
l.DataColor = BATTLENET_FONT_COLOR_CODE

l.Defaults = {}
l.Defaults.AddSpacer = false 				-- If a blank line should be added before loot info in tooltips
l.Defaults.MergeLists = false 				-- If we should show skins and loot in the same listing.
l.Defaults.ShowQuestItems = true 			-- If we should display items gathered for quests.
l.Defaults.AlwaysShowPicks = false 			-- If we should display pick pocketing results on Non-Rogues
l.Defaults.MobItemListLimit = 6 			-- Total Number of loots to show on a mob's tooltip.
l.Defaults.MobSkinListLimit = 6 			-- Total Number of skins to show on a mob's tooltip.
l.Defaults.ItemMobListLimit = 6 			-- Total Number of mob loots to show on an item's tooltip.
l.Defaults.SkinMobListLimit = 6 			-- Total Number of mob skins to show on an items's tooltip.
l.Defaults.ShowItemQualities = true 		-- If the tooltip should show the list of item qualities in the mob tooltip.
l.Defaults.DisplayCutOffLimit = 0 			-- Item Quality level to not display in tooltips.
l.Defaults.IncludeCurrencyInTotals = true	-- If currencies should be included in quality totals
l.Defaults.IncludeSkinsInTotals = true		-- If skins should be included in quality totals
l.Defaults.SeparateSkinData = true			-- If skinning data should be a separate listing when looking at item data.
l.Defaults.PercentPriorityLimitLoot = 10	-- The number of loots that are required to show higher on the tooltip.
l.Defaults.PercentPriorityLimitSkin = 10	-- The number of skins that are required to show higher on the tooltip.

l.SkinningSpellList = {
	-- Engineering
	[49383]		= "Engineering",
	-- Mining
	[32606] 	= "Mining",
	-- Herbalism
	[32605]		= "Herb Gathering",
	-- Skinning
	[8613]		= "Skinning", 		-- Apprentice
	[8617]		= "Skinning", 		-- Journeyman
	[8618]		= "Skinning",			-- Expert
	[10768]		= "Skinning", 		-- Artisan
	[32678]		= "Skinning", 		-- Master
	[50305]		= "Skinning", 		-- Grand Master
	[74522]		= "Skinning", 		-- Illustrious
	[102216]	= "Skinning",			-- Zen Master
	[158756]	= "Skinning",			-- Draenor
	[195125]	= "Skinning",			-- Legion
}
l.PickPocketSkillID = 921